/* Trial
 * Kinect SDK 1.6 and openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "testApp.h"

bool testApp::initKinect() {
	int numSensors;
	if (NuiGetSensorCount(&numSensors) < 0 || numSensors < 1) return false;
	if (NuiCreateSensorByIndex(0, &kinect) < 0) return false;

	kinect->NuiInitialize(NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_DEPTH);
	kinect->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_COLOR,
		NUI_IMAGE_RESOLUTION_640x480,
		0,    // Image stream flags
		2,    // Number of frames to buffer
		NULL, // Event handle
		&colorStream);
	kinect->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH,
		NUI_IMAGE_RESOLUTION_640x480,
		0,
		2,
		NULL,
		&depthStream);
	kinect->NuiCameraElevationSetAngle(0);
	kinect->NuiCameraElevationSetAngle(KINECT_TILT);

	return kinect;
}


//--------------------------------------------------------------
void testApp::setup(){
	if (!initKinect()) {
		ofLog() << "Kinect init error";
	}

	memset(alignedDepthData, 0, KINECT_WIDTH * KINECT_HEIGHT * 4);
	memset(alignedColorData, 0, KINECT_WIDTH * KINECT_HEIGHT * 4);

	colorTexture.allocate(KINECT_WIDTH, KINECT_HEIGHT, GL_RGBA);
	alignedDepthTexture.allocate(KINECT_WIDTH, KINECT_HEIGHT, GL_RGBA);
	alignedColorTexture.allocate(KINECT_WIDTH, KINECT_HEIGHT, GL_RGBA);
	depthTexture.allocate(KINECT_WIDTH, KINECT_HEIGHT, GL_RGBA);

	dejavu.loadFont("DejaVuSansMono.ttf", 12);
}

//--------------------------------------------------------------
void testApp::update(){
	HRESULT hr;
	NUI_IMAGE_FRAME imageFrame;

	// Get image stream data
	hr = kinect->NuiImageStreamGetNextFrame(colorStream, 0, &imageFrame);
	if (SUCCEEDED(hr)) {
		INuiFrameTexture* texture = imageFrame.pFrameTexture;
		NUI_LOCKED_RECT lockedRect;
		texture->LockRect(0, &lockedRect, NULL, 0);

		if (lockedRect.Pitch != 0) {
			memcpy(colorData, lockedRect.pBits, lockedRect.size);
		}

		texture->UnlockRect(0);
		kinect->NuiImageStreamReleaseFrame(colorStream, &imageFrame);
	}
	
	// Get depth stream data
	hr = kinect->NuiImageStreamGetNextFrame(depthStream, 0, &imageFrame);
	if (SUCCEEDED(hr)) {
		INuiFrameTexture* texture;
		NUI_LOCKED_RECT lockedRect;

		// Get packed depth
		texture = imageFrame.pFrameTexture;
		texture->LockRect(0, &lockedRect, NULL, 0);
		if (lockedRect.Pitch != 0) {
			memcpy(depthRaw, lockedRect.pBits, lockedRect.size);
		}
		texture->UnlockRect(0);

		// Get extended depth value
		BOOL nearMode = false;
		kinect->NuiImageFrameGetDepthImagePixelFrameTexture(
			depthStream,
			&imageFrame,
			&nearMode,
			&texture
			);
		texture->LockRect(0, &lockedRect, NULL, 0);

        const NUI_DEPTH_IMAGE_PIXEL* curr = reinterpret_cast<const NUI_DEPTH_IMAGE_PIXEL *>(lockedRect.pBits);
		const NUI_DEPTH_IMAGE_PIXEL* end = curr + (KINECT_WIDTH * KINECT_HEIGHT);
		USHORT* dest = depthMillimeters;

		if (lockedRect.Pitch != 0) {
			while (curr < end) {
				*dest++ = curr->depth;
				curr++;
			}
		}
		texture->UnlockRect(0);

		kinect->NuiImageStreamReleaseFrame(depthStream, &imageFrame);
	}

	// Get depth--color coordinate mapping
	kinect->NuiImageGetColorPixelCoordinateFrameFromDepthPixelFrameAtResolution(
		NUI_IMAGE_RESOLUTION_640x480,
		NUI_IMAGE_RESOLUTION_640x480,
		KINECT_WIDTH * KINECT_HEIGHT,
		depthRaw,
		KINECT_WIDTH * KINECT_HEIGHT * 2,
		colorCoordinates);
}

//--------------------------------------------------------------
void testApp::draw(){
	// Create a grayscale image of the depth:
	// B,G,R are all set to depth%256, alpha is set to 1
	BYTE* dest = depthData;
	for (int i = 0; i < (KINECT_WIDTH * KINECT_HEIGHT); i++) {
		// It's much faster to just shift right the depth value
		//   instead of calling NuiDepthPixelToDepth()
		// USHORT depth = depthRaw[i] >> NUI_IMAGE_PLAYER_INDEX_SHIFT;
		USHORT depth = depthMillimeters[i];
		*dest++ = 0; // (BYTE) depth % 256;
		*dest++ = (BYTE) depth % 256;
		*dest++ = (BYTE) depth % 256;
		*dest++ = 0x20;
	}

	// Align depth image to the color image
	// and align color image to the depth image
	BYTE* currDepthData = depthData;
	USHORT* currDepthMillimeters = depthMillimeters;
	for (int i = 0; i < KINECT_WIDTH * KINECT_HEIGHT * 2; i += 2) {
		int indexDepth = 4 * (colorCoordinates[i] + colorCoordinates[i + 1] * KINECT_WIDTH);

		alignedDepthData[indexDepth] = *currDepthData++;
		alignedDepthData[indexDepth+1] = *currDepthData++;
		alignedDepthData[indexDepth+2] = *currDepthData++;
		alignedDepthData[indexDepth+3] = *currDepthData++;
		alignedDepthMillimeters[indexDepth/4] = *currDepthMillimeters++;		
	}
	
	// Draw image frame to the window
	colorTexture.loadData(colorData, KINECT_WIDTH, KINECT_HEIGHT, GL_BGRA);
	colorTexture.draw(0, 0);
	
	// Draw aligned depth frame to the window
	alignedDepthTexture.loadData(alignedDepthData, KINECT_WIDTH, KINECT_HEIGHT, GL_BGRA);
	ofEnableAlphaBlending();
	alignedDepthTexture.draw(0, 0);
	ofDisableAlphaBlending();
	
	dejavu.drawString("FPS:" + ofToString(ofGetFrameRate()), 40, 40);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	if (key == 't') {
		LONG currentTilt;
		kinect->NuiCameraElevationGetAngle(&currentTilt);
		ofLog() << "Tilt: " << currentTilt;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	if (x > KINECT_WIDTH || y > KINECT_HEIGHT) return;
	ofLog() << "(" << x << ", " << y << "): " << alignedDepthMillimeters[y*KINECT_WIDTH + x];
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}